FROM docker.io/library/debian:bookworm-20240926-slim AS base


ENV PERL_CPANM_OPT "--from https://cpan.metacpan.org/"

RUN apt-get -y update \
    && apt-get -y install build-essential curl libtext-csv-perl libjson-perl libfile-slurp-perl libio-stringy-perl libclass-data-inheritable-perl libdevel-stacktrace-perl libtest-deep-perl libtest-differences-perl libtest-exception-perl libexception-class-perl libtest-warn-perl \
    && apt-get clean

RUN cd /usr/local/bin && curl -LO http://xrl.us/cpanm && chmod a+rx cpanm

RUN ["cpanm", "https://cpan.metacpan.org/authors/id/B/BA/BABF/Text-CSV-Slurp-1.03.tar.gz"]

COPY add-id.pl /usr/local/bin/
RUN chmod a+rx /usr/local/bin/add-id.pl
RUN /usr/local/bin/add-id.pl -h
ENTRYPOINT ["/usr/local/bin/add-id.pl"]
