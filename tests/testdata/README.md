Testdata from:

1. `mc cp pipeline/pipeline/pipeline/2024-02-21/logs/scraping.out.gz /tmp`
2. `zcat /tmp/scraping.out.gz | jq -c '.[] | { originalJobPosting:.}' | head -n 10 | sort > input.10`
3. I manually filtered out names etc.
4. The "expected output" was created by running the application on the input from above, sorting and storing:
```
podman run --rm -i -v $PWD/add-id.pl:/usr/local/bin/add-id.pl slask expected_db < input.10 | sort > expected_output.10
```
