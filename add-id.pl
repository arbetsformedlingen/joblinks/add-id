#!/usr/bin/env perl
use strict;
use warnings;
use Digest::MD5 qw(md5_hex);
use Encode;
use File::Slurp;
use JSON;
use POSIX qw(strftime);
use Text::CSV::Slurp;
use utf8;
use 5.012;

# Check arguments call calc_id when script is runned standalone
if (grep { $_ eq '-h' } @ARGV) {
    print "Usage: add-id.pl [db_file]\n";
    print "Reads JobLinks JSON objects from STDIN, adds an 'id' and 'firstSeen' field,\n";
    print "and updates the CSV db file with new IDs.\n";
    print "Options:\n  -h    Display this help message.\n";
    exit 0;
}
my $num_args = $#ARGV+1;
if ($num_args == 2) {
    if($ARGV[0] ne '--id'){
      print "\nUsage: add-id.pl [file] [-id URL]\n";
      exit;
    }
  print "URL '$ARGV[1]'\n";
  my $hash = calc_id($ARGV[1]);
  print "ID: '$hash'\n";
  exit;
}

my $db = $ARGV[0] or die "Need to get CSV file on the command line\n";

if (not defined $db){
  die "Need to get CSV file on the command line\n";
}

my %id2date = ();

if (-f $db && -s $db) {
    my $data = Text::CSV::Slurp->load(file => $db) or die "Failed to load CSV file: $db\n";
    foreach my $row (@{$data}) {
        $id2date{$row->{id}} = $row->{firstSeen};
    }
}

while (<STDIN>) {
    chomp;
    next unless $_;  # skip empty lines
    my $json = decode_json $_;
    die "Error: originalJobPosting URL is missing\n" unless defined $json->{originalJobPosting}->{url};
    my $cur_id = calc_id($json->{originalJobPosting}->{url});
    $id2date{$cur_id} = strftime "%Y-%m-%dT%X", localtime unless exists $id2date{$cur_id};
    $json->{id} = $cur_id;
    $json->{firstSeen} = $id2date{$cur_id};
    say encode_json $json;
}

my @csv = map { "\"$_\",\"$id2date{$_}\"" } sort keys %id2date;
unshift(@csv, '"id","firstSeen"');
my $csvdata = join("\n", @csv);
write_file $db, {binmode => ':utf8'}, $csvdata;

# plugin your url-to-id algo here.
sub calc_id {
    return md5_hex(utf8::is_utf8($_[0]) ? Encode::encode_utf8($_[0]) : $_[0]);
}
