# add-id - A JobLinks Pipeline Processor

## Overview
**add-id** is a tool designed to read JobLinks JSON objects from standard input, add a unique `id` and timestamp field (`firstSeen`) to each object, and update a CSV database file accordingly.

## Requirements
- [Docker](https://www.docker.com/)
- [Bash](https://www.gnu.org/software/bash/)
- Perl with required CPAN modules:
  - Digest::MD5
  - Encode
  - File::Slurp
  - JSON
  - POSIX
  - Text::CSV::Slurp

## Quick Start Guide
1. **Clone the repository:**
   ```bash
   git clone <repository-url>
   cd <repository-directory>
   ```

2. **Build the Docker image:**
   ```bash
   ./run.sh --build
   ```

3. **Run the application:**
   The container expects JobLinks JSON input from STDIN. For example:
   ```bash
   cat tests/testdata/input.10 | docker run --rm -i add-id
   ```

4. **Display help:**
   To see usage information:
   ```bash
   docker run --rm add-id -h
   ```

## Build Instructions
- Use the provided `run.sh` script to build the Docker image.
- The Dockerfile includes a test layer that runs the application with the `-h` flag to ensure proper startup.

## Usage
- The application reads JSON objects from STDIN.
- It updates each JSON object with a unique `id` and `firstSeen` timestamp.
- The updated JSON is written to STDOUT and the CSV database file is updated with new entries.

## Tests
- The test data is located in `tests/testdata/`.
- To verify output consistency, compare the application output against `tests/testdata/expected_output.10`.

## Configuration
- The CSV database file path is specified as a command-line argument.
- Additional options can be invoked using standard flags (e.g., `-h` for help).

## License
This project is licensed under the Apache License 2.0. See the [LICENSE](LICENSE) file for details.

## Contributing
Feel free to fork the repository and submit pull requests. Please adhere to the coding standards already present in the repository and include tests for any changes.

## Contact
Arbetsförmedlingen - https://arbetsformedlingen.se/kontakt